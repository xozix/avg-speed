# Readme


After clone/download please use "make firstrun" command to initialize project.

## make commands: 

firstrun - initialize application for the first time

run - initialize app

stop - stop app

restart - restart app

## run project

There are 2 ways to run project: 

1. go into app container and run ```/var/www/html/avg-speed-s4/bin/console app:avg-speed```
2. localhost:8100 (print console command output in browser)