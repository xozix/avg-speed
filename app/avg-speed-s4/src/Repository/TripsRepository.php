<?php

namespace App\Repository;

use App\Entity\Trips;
use App\Interfaces\Repository\TripsRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Trips|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trips|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trips[]    findAll()
 * @method Trips[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class TripsRepository extends ServiceEntityRepository implements TripsRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Trips::class);
    }
}
