<?php

namespace App\Interfaces\Model;

/**
 * Interface TripRowFactoryInterface
 */
interface TripRowFactoryInterface
{
    /**
     * @return TripRowInterface
     */
    public function create(): TripRowInterface;
}
