<?php

namespace App\Interfaces\Model;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface TripFormatterInterface
 */
interface TripFormatterInterface
{
    /**
     * @param OutputInterface $output
     * @param array           $tripsData
     *
     * @return mixed
     */
    public function showListing(OutputInterface $output, array $tripsData);
}
