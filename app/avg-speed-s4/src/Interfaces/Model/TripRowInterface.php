<?php

namespace App\Interfaces\Model;

/**
 * Interface TripRowInterface
 */
interface TripRowInterface
{
    /**
     * @param string|null $trip
     *
     * @return TripRowInterface
     */
    public function setTrip(?string $trip): TripRowInterface;

    /**
     * @param float|null $distance
     *
     * @return TripRowInterface
     */
    public function setDistance(?float $distance): TripRowInterface;

    /**
     * @param int|null $measureInterval
     *
     * @return TripRowInterface
     */
    public function setMeasureInterval(?int $measureInterval): TripRowInterface;

    /**
     * @param int|null $avgSpeed
     *
     * @return TripRowInterface
     */
    public function setAvgSpeed(?int $avgSpeed): TripRowInterface;

    /**
     * @return string|null
     */
    public function getTrip(): ?string;

    /**
     * @return float|null
     */
    public function getDistance(): ?float;

    /**
     * @return int|null
     */
    public function getMeasureInterval(): ?int;

    /**
     * @return int|null
     */
    public function getAvgSpeed(): ?int;
}
