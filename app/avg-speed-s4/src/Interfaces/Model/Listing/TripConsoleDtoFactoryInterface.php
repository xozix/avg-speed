<?php

namespace App\Interfaces\Model\Listing;

/**
 * Interface TripConsoleDtoFactoryInterface
 */
interface TripConsoleDtoFactoryInterface
{
    /**
     * @return TripConsoleDtoInterface
     */
    public function create(): TripConsoleDtoInterface;
}
