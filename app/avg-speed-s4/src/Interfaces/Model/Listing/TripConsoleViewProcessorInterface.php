<?php

namespace App\Interfaces\Model\Listing;

use App\Interfaces\Model\TripRowInterface;

/**
 * Interface TripConsoleViewProcessorInterface
 */
interface TripConsoleViewProcessorInterface
{
    /**
     * Listing table console visualisation chars
     */
    const LINE_CHAR = '-';
    const COLUMN_BREAK_CHAR = '+';
    const COLUMN_SEPARATOR_CHAR = '|';

    /**
     * Listing column names
     */
    const  TRIP_COLUMN             = 'trip';
    const  DISTANCE_COLUMN         = 'distance';
    const  MEASURE_INTERVAL_COLUMN = 'measure interval';
    const  AVG_SPEED_COLUMN        = 'avg speed';

    /**
     * @param TripRowInterface[] $data
     *
     * @return TripConsoleDtoInterface
     */
    public function getConsoleListing(array $data): TripConsoleDtoInterface;
}
