<?php

namespace App\Interfaces\Model\Listing;

/**
 * Interface TripConsoleDtoInterface
 */
interface TripConsoleDtoInterface
{
    /**
     * @param string|null $rowBreakLine
     *
     * @return TripConsoleDtoInterface
     */
    public function setRowBreakLine(?string $rowBreakLine): TripConsoleDtoInterface;

    /**
     * @param string|null $rowHeaderData
     *
     * @return TripConsoleDtoInterface
     */
    public function setRowHeaderData(?string $rowHeaderData): TripConsoleDtoInterface;

    /**
     * @param array|null $dataRows
     *
     * @return TripConsoleDtoInterface
     */
    public function setDataRows(?array $dataRows): TripConsoleDtoInterface;

    /**
     * @return string|null
     */
    public function getRowBreakLine(): ?string;

    /**
     * @return string|null
     */
    public function getRowHeaderData(): ?string;

    /**
     * @return array|null
     */
    public function getDataRows(): ?array;
}
