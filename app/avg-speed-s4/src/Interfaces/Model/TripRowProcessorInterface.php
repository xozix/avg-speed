<?php

namespace App\Interfaces\Model;

use App\Entity\Trips as TripsEntity;

/**
 * Interface TripRowProcessorInterface
 */
interface TripRowProcessorInterface
{
    /**
     * @param TripsEntity $trip
     *
     * @return TripRowInterface
     */
    public function getTripViewRowData(TripsEntity $trip): TripRowInterface;
}
