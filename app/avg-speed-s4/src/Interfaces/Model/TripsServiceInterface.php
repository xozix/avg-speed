<?php

namespace App\Interfaces\Model;

/**
 * Interface TripsServiceInterface
 */
interface TripsServiceInterface
{
    /**
     * @return TripRowInterface[]
     */
    public function getTripsData(): array;
}
