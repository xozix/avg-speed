<?php

namespace App\Interfaces\Model;

/**
 * Interface TripsProcessorInterface
 */
interface TripsProcessorInterface
{
    /**
     * @return TripRowInterface[]
     */
    public function getTripsViewRowsData(): array;
}
