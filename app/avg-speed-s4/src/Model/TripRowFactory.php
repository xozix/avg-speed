<?php

namespace App\Model;

use App\Interfaces\Model\TripRowFactoryInterface;
use App\Interfaces\Model\TripRowInterface;

/**
 * Class TripRowFactory
 */
class TripRowFactory implements TripRowFactoryInterface
{
    /**
     * @var TripRowInterface
     */
    private $tripRow;

    /**
     * TripRowFactory constructor.
     *
     * @param TripRowInterface $tripRow
     */
    public function __construct(TripRowInterface $tripRow)
    {
        $this->tripRow = $tripRow;
    }

    /**
     * @return TripRowInterface
     */
    public function create(): TripRowInterface
    {
        return clone $this->tripRow;
    }
}
