<?php

namespace App\Model\Listing;

use App\Interfaces\Model\Listing\TripConsoleDtoFactoryInterface;
use App\Interfaces\Model\Listing\TripConsoleDtoInterface;
use App\Interfaces\Model\Listing\TripConsoleViewProcessorInterface;
use App\Interfaces\Model\TripRowInterface;

/**
 * Class TripConsoleViewProcessor
 */
class TripConsoleViewProcessor implements TripConsoleViewProcessorInterface
{
    /**
     * @var TripConsoleDtoFactoryInterface
     */
    private $consoleDtoFactory;

    /**
     * TripConsoleViewProcessor constructor.
     *
     * @param TripConsoleDtoFactoryInterface $consoleDtoFactory
     */
    public function __construct(TripConsoleDtoFactoryInterface $consoleDtoFactory)
    {
        $this->consoleDtoFactory = $consoleDtoFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsoleListing(array $data): TripConsoleDtoInterface
    {
        $dto = $this->consoleDtoFactory->create();
        $columnsWidth = $this->getColumnWidth($data);

        $rowBreakLine = self::COLUMN_BREAK_CHAR;
        $rowHeaderData = self::COLUMN_SEPARATOR_CHAR;

        foreach ($columnsWidth as $name => $width) {
            $rowBreakLine .= str_repeat(self::LINE_CHAR, $width) . self::COLUMN_BREAK_CHAR;
            $rowHeaderData .= $this->formatCell($name, $width, STR_PAD_RIGHT) . self::COLUMN_SEPARATOR_CHAR;
        }

        $dto->setRowBreakLine($rowBreakLine);
        $dto->setRowHeaderData($rowHeaderData);

        $dataRows = [];
        /** @var TripRowInterface $tripRow */
        foreach ($data as $tripRow) {
            $dataRows[] = self::COLUMN_SEPARATOR_CHAR . implode(
                    self::COLUMN_SEPARATOR_CHAR, [
                        $this->formatCell($tripRow->getTrip(), $columnsWidth[self::TRIP_COLUMN]),
                        $this->formatCell($tripRow->getDistance(), $columnsWidth[self::DISTANCE_COLUMN]),
                        $this->formatCell($tripRow->getMeasureInterval(), $columnsWidth[self::MEASURE_INTERVAL_COLUMN]),
                        $this->formatCell($tripRow->getAvgSpeed(), $columnsWidth[self::AVG_SPEED_COLUMN])
                    ]
                ) . self::COLUMN_SEPARATOR_CHAR;
        }
        $dto->setDataRows($dataRows);

        return $dto;
    }

    /**
     * @param TripRowInterface[] $data
     *
     * @return array
     */
    protected function getColumnWidth(array $data)
    {
        $columnsWidth = [
            self::TRIP_COLUMN             => strlen(self::TRIP_COLUMN),
            self::DISTANCE_COLUMN         => strlen(self::DISTANCE_COLUMN),
            self::MEASURE_INTERVAL_COLUMN => strlen(self::MEASURE_INTERVAL_COLUMN),
            self::AVG_SPEED_COLUMN        => strlen(self::AVG_SPEED_COLUMN),
        ];

        /** @var TripRowInterface $tripViewRow */
        foreach ($data as $tripViewRow) {
            $columnsWidth[self::TRIP_COLUMN] = max(
                [
                    strlen($tripViewRow->getTrip()),
                    $columnsWidth[self::TRIP_COLUMN]
                ]
            );
            $columnsWidth[self::DISTANCE_COLUMN] = max(
                [
                    strlen($tripViewRow->getDistance()),
                    $columnsWidth[self::DISTANCE_COLUMN]
                ]
            );
            $columnsWidth[self::MEASURE_INTERVAL_COLUMN] = max(
                [
                    strlen($tripViewRow->getMeasureInterval()),
                    $columnsWidth[self::MEASURE_INTERVAL_COLUMN]
                ]
            );
            $columnsWidth[self::AVG_SPEED_COLUMN] = max(
                [
                    strlen($tripViewRow->getAvgSpeed()),
                    $columnsWidth[self::AVG_SPEED_COLUMN]
                ]
            );
        }
        //increase final width by spaces on both sides
        foreach ($columnsWidth as $column => &$width) {
            $width += 2;
        }

        return $columnsWidth;
    }

    /**
     * @param string $data
     * @param int    $width
     * @param int    $padType
     *
     * @return string
     */
    protected function formatCell(string $data, int $width, $padType = STR_PAD_LEFT)
    {
        return str_pad(' ' . $data . ' ', $width, " ", $padType);
    }
}
