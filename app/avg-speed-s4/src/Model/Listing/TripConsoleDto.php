<?php

namespace App\Model\Listing;

use App\Interfaces\Model\Listing\TripConsoleDtoInterface;

/**
 * Class TripConsoleDto
 */
class TripConsoleDto implements TripConsoleDtoInterface
{
    /**
     * @var string|null
     */
    private $rowBreakLine;

    /**
     * @var string|null
     */
    private $rowHeaderData;

    /**
     * @var array|null
     */
    private $dataRows;

    /**
     * {@inheritdoc}
     */
    public function setRowBreakLine(?string $rowBreakLine): TripConsoleDtoInterface
    {
        $this->rowBreakLine = $rowBreakLine;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRowHeaderData(?string $rowHeaderData): TripConsoleDtoInterface
    {
        $this->rowHeaderData = $rowHeaderData;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setDataRows(?array $dataRows): TripConsoleDtoInterface
    {
        $this->dataRows = $dataRows;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRowBreakLine(): ?string
    {
        return $this->rowBreakLine;
    }

    /**
     * {@inheritdoc}
     */
    public function getRowHeaderData(): ?string
    {
        return $this->rowHeaderData;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataRows(): ?array
    {
        return $this->dataRows;
    }
}
