<?php

namespace App\Model\Listing;

use App\Interfaces\Model\Listing\TripConsoleDtoFactoryInterface;
use App\Interfaces\Model\Listing\TripConsoleDtoInterface;

/**
 * Class TripConsoleDtoFactory
 */
class TripConsoleDtoFactory implements TripConsoleDtoFactoryInterface
{
    /**
     * @var TripConsoleDtoInterface
     */
    private $tripConsoleDto;

    /**
     * TripConsoleDtoFactory constructor.
     *
     * @param TripConsoleDtoInterface $tripConsoleDto
     */
    public function __construct(TripConsoleDtoInterface $tripConsoleDto)
    {
        $this->tripConsoleDto = $tripConsoleDto;
    }

    /**
     * {@inheritdoc}
     */
    public function create(): TripConsoleDtoInterface
    {
        return clone $this->tripConsoleDto;
    }
}
