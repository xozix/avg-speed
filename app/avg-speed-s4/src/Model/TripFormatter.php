<?php

namespace App\Model;

use App\Interfaces\Model\Listing\TripConsoleViewProcessorInterface;
use App\Interfaces\Model\TripFormatterInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TripFormatter
 */
class TripFormatter implements TripFormatterInterface
{
    /**
     * @var TripConsoleViewProcessorInterface
     */
    private $consoleViewProcessor;

    /**
     * TripFormatter constructor.
     *
     * @param TripConsoleViewProcessorInterface $consoleViewProcessor
     */
    public function __construct(TripConsoleViewProcessorInterface $consoleViewProcessor)
    {
        $this->consoleViewProcessor = $consoleViewProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function showListing(OutputInterface $output, array $tripsData)
    {
        $listingDto = $this->consoleViewProcessor->getConsoleListing($tripsData);

        $output->writeln($listingDto->getRowBreakLine());
        $output->writeln($listingDto->getRowHeaderData());
        $output->writeln($listingDto->getRowBreakLine());

        foreach ($listingDto->getDataRows() as $dataRow) {
            $output->writeln($dataRow);
        }

        $output->writeln($listingDto->getRowBreakLine());
    }
}
