<?php

namespace App\Model;

/**
 * Trait AvgSpeedTrait
 */
trait AvgSpeedTrait
{
    /**
     * @param float $delta
     * @param int   $measureInterval
     *
     * @return int
     */
    public function getAvgSpeed(float $delta, int $measureInterval): int
    {
        return (int) ((3600*$delta)/$measureInterval);
    }
}
