<?php

namespace App\Model;

use App\Entity\TripMeasures;
use App\Entity\Trips as TripsEntity;
use App\Interfaces\Model\TripRowFactoryInterface;
use App\Interfaces\Model\TripRowInterface;
use App\Interfaces\Model\TripRowProcessorInterface;
use Doctrine\ORM\PersistentCollection;

/**
 * Class TripRowProcessor
 */
class TripRowProcessor implements TripRowProcessorInterface
{
    use AvgSpeedTrait;

    /**
     * @var TripRowFactoryInterface
     */
    private $tripRowFactory;

    /**
     * TripViewRowProcessor constructor.
     *
     * @param TripRowFactoryInterface $tripRowFactory
     */
    public function __construct(TripRowFactoryInterface $tripRowFactory) {
        $this->tripRowFactory = $tripRowFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getTripViewRowData(TripsEntity $trip): TripRowInterface
    {
        return $this->tripRowFactory->create()
            ->setTrip($trip->getName())
            ->setMeasureInterval($trip->getMeasureInterval())
            ->setDistance($this->getTripRowDistance($trip->getTripMeasures()))
            ->setAvgSpeed($this->getTripRowAvgSpeed($trip->getTripMeasures(), $trip->getMeasureInterval()));
    }

    /**
     * @param PersistentCollection $tripMeasures
     *
     * @return float
     */
    protected function getTripRowDistance(PersistentCollection $tripMeasures): float
    {
        if ($tripMeasures->isEmpty()) {
            return 0.0;
        }
        return (float) $tripMeasures->last()->getDistance() - $tripMeasures->first()->getDistance();
    }

    /**
     * @param PersistentCollection $tripMeasures
     * @param int                  $measureInterval
     *
     * @return int
     */
    protected function getTripRowAvgSpeed(PersistentCollection $tripMeasures, int $measureInterval): int
    {
        if ($tripMeasures->isEmpty()) {
            return 0;
        }

        $distance = null;
        $maxSpeed = 0;

        /** @var TripMeasures $measure */
        foreach ($tripMeasures as $measure) {
            if (is_null($distance)) {
                $distance = $measure->getDistance();
                continue;
            }
            $delta = $measure->getDistance() - $distance;
            $avgSpeed = $this->getAvgSpeed($delta, $measureInterval);
            $distance = $measure->getDistance();
            $maxSpeed = ($avgSpeed > $maxSpeed) ? $avgSpeed : $maxSpeed;
        }

        return $maxSpeed;
    }
}
