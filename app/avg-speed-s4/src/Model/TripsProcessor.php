<?php

namespace App\Model;

use App\Interfaces\Model\TripsProcessorInterface;
use App\Interfaces\Repository\TripsRepositoryInterface;
use App\Interfaces\Model\TripRowProcessorInterface;

/**
 * Class TripsProcessor
 */
class TripsProcessor implements TripsProcessorInterface
{
    /**
     * @var TripsRepositoryInterface
     */
    private $tripsRepository;

    /**
     * @var TripRowProcessorInterface
     */
    private $tripViewRowProcessor;

    /**
     * TripsProcessor constructor.
     *
     * @param TripsRepositoryInterface  $tripsRepository
     * @param TripRowProcessorInterface $tripViewRowProcessor
     */
    public function __construct(
        TripsRepositoryInterface $tripsRepository,
        TripRowProcessorInterface $tripViewRowProcessor
    ) {
        $this->tripsRepository = $tripsRepository;
        $this->tripViewRowProcessor = $tripViewRowProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function getTripsViewRowsData(): array
    {
        $result = [];
        foreach ($this->tripsRepository->findAll() as $trip) {
            $result[] = $this->tripViewRowProcessor->getTripViewRowData($trip);
        }

        return $result;
    }
}
