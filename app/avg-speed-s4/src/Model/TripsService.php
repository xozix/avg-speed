<?php

namespace App\Model;

use App\Interfaces\Model\TripsProcessorInterface;
use App\Interfaces\Model\TripsServiceInterface;

/**
 * Class TripsService
 */
class TripsService implements TripsServiceInterface
{
    /**
     * @var TripsProcessorInterface
     */
    private $tripsProcessor;

    /**
     * TripsService constructor.
     *
     * @param TripsProcessorInterface $tripsProcessor
     */
    public function __construct(TripsProcessorInterface $tripsProcessor)
    {
        $this->tripsProcessor = $tripsProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function getTripsData(): array
    {
        return $this->tripsProcessor->getTripsViewRowsData();
    }
}
