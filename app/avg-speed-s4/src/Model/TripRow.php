<?php

namespace App\Model;

use App\Interfaces\Model\TripRowInterface;

/**
 * Class TripRow
 */
class TripRow implements TripRowInterface
{
    /**
     * @var string|null
     */
    private $trip;

    /**
     * @var float|null
     */
    private $distance;

    /**
     * @var int|null
     */
    private $measureInterval;

    /**
     * @var int|null
     */
    private $avgSpeed;

    /**
     * {@inheritdoc}
     */
    public function setTrip(?string $trip): TripRowInterface
    {
        $this->trip = $trip;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setDistance(?float $distance): TripRowInterface
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMeasureInterval(?int $measureInterval): TripRowInterface
    {
        $this->measureInterval = $measureInterval;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAvgSpeed(?int $avgSpeed): TripRowInterface
    {
        $this->avgSpeed = $avgSpeed;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTrip(): ?string
    {
        return $this->trip;
    }

    /**
     * {@inheritdoc}
     */
    public function getDistance(): ?float
    {
        return $this->distance;
    }

    /**
     * {@inheritdoc}
     */
    public function getMeasureInterval(): ?int
    {
        return $this->measureInterval;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvgSpeed(): ?int
    {
        return $this->avgSpeed;
    }
}
