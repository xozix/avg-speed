<?php

namespace App\Console\Command;

use App\Interfaces\Model\TripFormatterInterface;
use App\Interfaces\Model\TripsServiceInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AvgSpeed
 */
class AvgSpeed extends SymfonyCommand
{
    const CONFIG_HELP = 'Derp - help';
    const CONFIG_DESCRIPTION = 'Show me the preciousss!';

    protected static $defaultName = 'app:avg-speed';

    /**
     * @var TripsServiceInterface
     */
    private $tripsService;

    /**
     * @var TripFormatterInterface
     */
    private $tripFormatter;

    /**
     * AvgSpeed constructor.
     *
     * @param TripsServiceInterface  $tripsService
     * @param TripFormatterInterface $tripFormatter
     */
    public function __construct(
        TripsServiceInterface $tripsService,
        TripFormatterInterface $tripFormatter
    ) {
        parent::__construct();

        $this->tripsService = $tripsService;
        $this->tripFormatter = $tripFormatter;
    }

    /**
     * Config console command
     */
    public function configure()
    {
        $this->setDescription(self::CONFIG_DESCRIPTION)
            ->setHelp(self::CONFIG_HELP);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = $this->tripsService->getTripsData();
        $this->tripFormatter->showListing($output, $data);
    }
}
