default:
	@echo "Available commands: firstrun, run , stop, restart"
# first run app
firstrun:
	make run
	docker exec -it test_str_app bash -c "cd avg-speed-s4 && composer install"
# run app
run:
	docker-compose up -d
# stop app
stop:
	@docker-compose stop
# restart app
restart: stop run
